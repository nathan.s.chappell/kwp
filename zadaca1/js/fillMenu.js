// fillMenu.js

class Action {
  constructor(name,image,link) {
    this.name = name;
    this.image = image;
    this.link = link;
  }

  make_span() {
    let span = document.createElement("span");
    span.innerHTML = this.name;
    return span;
  }

  make_img() {
    let img = document.createElement("img");
    img.className = "left-panel-list-img";
    img.src = this.image.name;
    img.alt = this.image.alt;
    return img;
  }

  createElement() {
    let a = document.createElement("a");
    a.href = this.link;
    a.append(this.make_img(), this.make_span());
    return a;
  }
}

let actions = [
  new Action("Inventura",{name:"img/News.svg",alt:"Inventura"},"#Inventura"),
  new Action("Inventar",{name:"img/Box.svg",alt:"Inventar"},"#Inventar"),
  new Action("Prostorije",{name:"img/Classroom.svg",alt:"Prostorije"},"#Prostorije"),
  new Action("Zaposlenici",{name:"img/Contacts.svg",alt:"Zaposlenici"},"#Zaposlenici"),
  new Action("Administracija",{name:"img/Services.svg",alt:"Administracija"},"#Administracija"),
];

function getActionList() {
  let ul = document.createElement("ul");
  for (let i = 0; i < actions.length; ++i) {
    let li = document.createElement("li");
    li.appendChild(actions[i].createElement());
    ul.appendChild(li);
  }
  return ul;
}

function addActionList() {
  let leftPanel = document.getElementById("left-panel");
  leftPanel.appendChild(getActionList());
}

addActionList();
