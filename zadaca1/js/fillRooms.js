// fillRooms.js

class Room {
  constructor(name,itemsCount) {
    this.name = name;
    this.itemsCount = itemsCount;
  }

  make_a() {
    let a = document.createElement('a');
    a.href = "#";
    let p_name = document.createElement('p');
    p_name.innerHTML = this.name;
    let p_itemsCount = document.createElement('p');
    p_itemsCount.innerHTML = this.itemsCount;
    p_itemsCount.className = 'mid-small';
    a.append(p_name,p_itemsCount);
    return a;
  }

  createElement() {
    let div = document.createElement('div');
    div.classList.add('mid-item','thin-bottom-border','roomElement');
    div.append(this.make_a());
    return div;
  }
}

rooms = [
  new Room("Predavaonica 1","Broj predmeta: 50"),
  new Room("Predavaonica 2","Broj predmeta: 50"),
  new Room("Predavaonica 3","Broj predmeta: 50"),
  new Room("Predavaonica 4","Broj predmeta: 50"),
  new Room("Predavaonica 5","Broj predmeta: 50"),
  new Room("Predavaonica 6","Broj predmeta: 50"),
  new Room("Portirnica","Broj predmeta: 50"),
  new Room("Referada","Broj predmeta: 50"),
];

function getMidPanel() { return document.getElementById('mid-panel'); }

function element_is_room(element) {
  return element.classList && 
         element.classList.contains('roomElement')
}

function addRoomList() {
  let midPanel = getMidPanel();
  for (let i = 0; i < rooms.length; ++i) {
    midPanel.append(rooms[i].createElement());
  }
}

function removeRoomList() {
  let midPanel = getMidPanel();
  for (let i = 0; i < midPanel.childNodes.length;/*iterate below*/) {
    let child = midPanel.childNodes[i];
    if (element_is_room(child)) midPanel.removeChild(child);
    else ++i;
  }
}

/*
 * note: to view the rooms in ascending order from top to bottom, sort
 * descending: they are pushed on stack-wise
 */
function sort_rooms_ascending() { rooms.sort((l,r) => l.name < r.name); }
function sort_rooms_descending() { rooms.sort((l,r) => l.name > r.name); }

sort_rooms_ascending();
let sorted_ascending = true;

/*
 * lazy hack: I don't care to dig into SVG right now, so I just made another
 * image with the arrow upside down.
 */
function swap_room_order() {
  removeRoomList();
  let orderArrow = document.getElementById('order-arrow');
  if (sorted_ascending) {
    sort_rooms_descending();
    addRoomList();
    orderArrow.src = "img/Arrow.svg";
  } else {
    sort_rooms_ascending();
    addRoomList();
    orderArrow.src = "img/upsideDownArrow.svg";
  }
  sorted_ascending = !sorted_ascending;
}

addRoomList();

document.getElementById('mid-order-link')
        .addEventListener('click',swap_room_order,false);
