// js/domPaths.js

function get_path_to_root(element) {
  path = [];
  while (element != null) {
    path.push(element);
    element = element.parentNode;
  }
  return path;
}

function join_paths(p1, p2) {
  i = p1.length-1;
  j = p2.length-1;
  if (p1[i] != p2[j]) return [];
  while (i > 0 && j > 0) {
    if (p1[--i] == p2[--j]) {
      p1.pop();
      p2.pop();
    } else {
      p1.pop();
      break;
    }
  }
  return p1.concat(p2.reverse());
}

prev_element = null;

function get_and_print_path(event) {
  console.log('click: ' + event.target.tagName)
  if (!prev_element) {
    prev_element = event.target;
  } else {
    // "elegant" way to log the joined paths
    console.log(
      join_paths(
        get_path_to_root(prev_element),
        get_path_to_root(event.target))
      .map(element => element.tagName)
      .reduce((acc,val) => `${acc} -> ${val}`));
    prev_element = null;
  }
}

document.addEventListener('click',get_and_print_path,true);
